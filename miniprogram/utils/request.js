
import config from '../utils/config'
export const request1=(params)=>{

  // 添加openid
  if(params.data == null) {
    params["data"] = {openid:wx.getStorageSync('openid')};
  }else {
    let data={...params.data};
    data["openid"] = wx.getStorageSync('openid');
    params.data= data;
  }
  console.log(params)

  // 判断 url中是否带有 /my/ 请求的是私有的路径 带上header token
  let header={...params.header};
  if(params.url.includes("/my/")){
    // 拼接header 带上token
    header["Authorization"]=wx.getStorageSync("token");
  }

  // 定义公共的url
  // const baseUrl="https://api-hmugo-web.itheima.net/api/public/v1";
  return new Promise((resolve,reject)=>{
    wx.request({
     ...params,
     header:header,
     url:config.baseUrl+params.url,
     success:(result)=>{
       console.log(result)
      if(result.data.code == 200) {
        resolve(result.data.data);
      }else if (result.data.code == 300) {
        wx.showToast({
          title: result.data.data,
          icon: 'none',
          duration:2000
          });
        wx.navigateTo({
          url: '/pages/bindInfo/bindInfo',
        })
      }else {
        wx.showToast({
          title: result.data.data,
          icon: 'none',
          duration:2000
          });
      }
     },
     fail:(err)=>{
       reject(err);
     }
    
    });
  })
}

