import config from '../utils/config'
// 同时发送异步代码的次数
let ajaxTimes=0;
export const request=(params)=>{

  // 添加openid
  if(params.data == null) {
    params["data"] = {openid:wx.getStorageSync('openid')};
  }else {
    let data={...params.data};
    data["openid"] = wx.getStorageSync('openid');
    params.data= data;
  }
  console.log(params)
  

  // 判断 url中是否带有 /my/ 请求的是私有的路径 带上header token
  let header={...params.header};
  if(params.url.includes("/my/")){
    // 拼接header 带上token
    header["Authorization"]=wx.getStorageSync("token");
  }

  ajaxTimes++;
  // 显示加载中 效果
  wx.showLoading({
    title: "加载中",
    mask: true
  });
    

  // 定义公共的url
  // const baseUrl="https://api-hmugo-web.itheima.net/api/public/v1";
  return new Promise((resolve,reject)=>{
    wx.request({
     ...params,
     header:header,
     url:config.baseUrl+params.url,
     success:(result)=>{
      wx.hideLoading();
       console.log(result)
      if(result.data.code == 200) {
        resolve(result.data.data);
      }else if (result.data.code == 300) {
        wx.showToast({
          title: result.data.data,
          icon: 'none',
          duration:2000
          });
        wx.navigateTo({
          url: '/pages/bindInfo/bindInfo',
        })
      }else {
        wx.showToast({
          title: result.data.data,
          icon: 'none',
          duration:2000
          });
      }
     },
     fail:(err)=>{
        wx.hideLoading();
        wx.stopPullDownRefresh();
       reject(err);
     },
     complete:()=>{
      // ajaxTimes--;
      // if(ajaxTimes===0){
      //   //  关闭正在等待的图标
      //   wx.hideLoading();
      // }
     }
    });
  })
}

