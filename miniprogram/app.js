// app.js
App({
  onLaunch() {
    // 展示本地存储能力
    // const logs = wx.getStorageSync('logs') || []
    // logs.unshift(Date.now())
    // wx.setStorageSync('logs', logs)

    var openid = wx.getStorageSync('openid')
    console.log("openid已存在:+",openid)
      // 登录
      wx.login({
        success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        console.log(res)
        var uni_code = wx.getStorageSync('uni_code')
        if(res){
          wx.request({
            url: this.globalData.baseUrl + '/api/getOpenId',
            data:{
              code:res.code,
              uni_code:uni_code?uni_code:""
            },
            success:(result)=>{
              console.log(result)
              wx.setStorageSync('openid', result.data.data)
            },
            fail:(error)=> {
              console.log(error)
            }
          })
        }
      }
      })
  
  },
    
  getUserProfile() {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },

  globalData: {
    userInfo: null,
    baseUrl:"https://ygx.bsn666.com"
  }
})
