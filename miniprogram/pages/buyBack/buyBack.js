// pages/buyBack/buyBack.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    value:"1",
    price:"",
    number:0,
  },

  onChange(event){
    console.log(event)
    this.setData({
      value:event.detail
    })
  },

  // 查询复购单价
  getFuGouPrice() {
    request({ url: "/api/fuGouPrice"})
    .then(result => {
      this.setData({
        price:result
      })
     
    })
  },

  // 查询数量
  getNumber() {
    request({ url: "/api/getNumber"})
    .then(result => {
      this.setData({
        number:result
      })
    })
  },


  // 立即购买
  onSubmit() {
    var that = this;
    request({ url: "/api/fuGouNow",data:{number:that.data.value}})
    .then(result => {
      that.getBuyNow(result)
    })
  },

   // 立即购买
   getBuyNow (orderId) {
    var that = this;
    request({ url: "/api/buyNow",
        data:{
          orderId:orderId,
          addressId:"-1",
             } 
    })
    .then(result => {
      console.log(result)
      wx.requestPayment({
        nonceStr: result.nonceStr,
        package: result.package,
        paySign: result.paySign,
        timeStamp: result.timeStamp,
        signType: result.signType,
        success (res){
          console.log(res)
          wx.showToast({
            title: "支付成功",
            icon: 'none',
            duration:2000
          });
          wx.navigateTo({
            url: '/pages/order/order?status=2'
          })
   
        },
        fail(err) {
          console.log(err)
          wx.showToast({
            title: "支付失败",
            icon: 'none',
            duration:2000
          });
        }
      })
      
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getFuGouPrice()
    this.getNumber()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})