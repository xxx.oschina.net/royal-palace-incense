// pages/address/address.js
import { request } from "../../utils/index.js";
import {Dialog } from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checked: true,
    show: false,
    addressList:[],

  },

  // 点击修改地址
  onEditor(event) {
    // 字典转json字符串
    let str=JSON.stringify(event.currentTarget.dataset.value);
    console.log(str)
    wx.navigateTo({
      url: '/pages/createAddress/createAddress?title=修改地址&jsonStr='+str,   
    })
  },

  // 点击默认地址
  onChange(event) {
    let value= event.currentTarget.dataset.value
    console.log("value："+value)
    this.addressChecked(value)
  },

  // 点击删除地址
  onDelete(event) {
    let value= event.currentTarget.dataset.value
    console.log("value："+value)
    this.addressDel(value)
    // this.setData({show:true})
  },

  onConfirm(event) {
    console.log(event.detail);
  },

  onClose() {
    this.setData({ show: false });
  },


  // 添加收货地址
  clickCreateAddress() {
    wx.navigateTo({
      url: '/pages/createAddress/createAddress?title=添加地址',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

 
  },

  getAddressList () {
    request({ url: "/api/addressList" })
    .then(result => {
      console.log(result)
      this.setData({
        addressList: result.data
      })
    })
  },
  
  // 设置默认地址
  addressChecked (id) {
    request({ url: "/api/addressChecked",data:{id:id} 
    })
    .then(result => {
       this.getAddressList()      
    })
  },

  // 删除地址
  addressDel (id) {
    request({ url: "/api/addressDel",data:{id:id} })
    .then(result => {
       this.getAddressList()      
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    // 查询地址列表数据
    this.getAddressList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
     //调用刷新时将执行的方法
     this.onRefresh();
  },

 //刷新
 onRefresh(){
  //在当前页面显示导航条加载动画
  // wx.showNavigationBarLoading(); 
  //显示 loading 提示框。需主动调用 wx.hideLoading 才能关闭提示框
  // wx.showLoading({
  //   title: '刷新中...',
  // })
  // this.getAddressList();
},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})