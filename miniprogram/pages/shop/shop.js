// pages/shop/shop.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    value: '', // 搜索内容
    goodsUrl: getApp().globalData.baseUrl + "/images/goods/",
    cateList:[
      {
        state:"0",
        title:"美妆个护",
      },
      {
        state:"1",
        title:"红参饮品",
      },
      {
        state:"2",
        title:"1596元套餐",
      },
      {
        state:"3",
        title:"2980元套餐",
      },
    ],
    goodsList : [],
    page:1,

  },

  onChange(e) {
    console.log(e)
    // this.setData({
    //   value: e.detail,
    // });
  }, 
  onSearch(event) {
    // Toast('搜索' + this.data.value);
    console.log(event)
    this.data.page = 1;
    this.data.goodsList = [];
    this.getGoodsList(event.detail)
  },
  onClick(event) {
    // Toast('搜索' + this.data.value);
    console.log(event)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },

  getGoodsList (title) {
    var that = this;
    request({ url: "/api/searchGoods",data:{title:title,page:that.data.page}})
    .then(result => {
      console.log(result)
      that.setData({
        // goodsList: result.data
        goodsList:that.data.goodsList.concat(result.data)
      })
    })
  },

   // 点击商品
   onItmeClick (item){
    // let str=JSON.stringify(item.currentTarget.dataset.value);
    let goodsId = item.currentTarget.dataset.value.id;
    wx.navigateTo({
      // url: '/pages/goodsDetail/goodsDetail?jsonStr='+encodeURIComponent(str)
      url: '/pages/goodsDetail/goodsDetail?goodsId='+goodsId
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 商品列表数据
    this.data.page = 1;
    this.data.goodsList = [];
    this.getGoodsList("")
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.page++;
    console.log(this.data.page)
    this.getGoodsList("");
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})