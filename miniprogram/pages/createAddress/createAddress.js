// pages/createAddress/createAddress.js 
import { request } from "../../utils/index.js";
import { areaList} from "../../utils/areaList.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    areaList:{},  // 选中省市区数据
    checked: true, // 默认地址是否选中
    type:0, // 0:添加地址 1:修改修改
    show: false,
    province:"",
    city:"",
    county:"",
    userInfo:{
      province:"",
      city:"",
      county:"",
      addressee:"",
      info:"",
      phone:"",
      lat:"",
      lng:"",
      is_check:null
    },
  },

  onChangeName(event){
    var that = this;
    that.data.userInfo.addressee = event.detail;
  },

  onChangePhone(event) {
    var that = this;
    that.data.userInfo.phone = event.detail;
    console.log(that.data.userInfo.phone)
  },

  onChangeAddress(event) {
    var that = this;
    that.data.userInfo.info = event.detail;
  },

  // 选择地址
  showPopup() {
    this.setData({ show: true });
  },

  // 关闭选择地址
  onClose() {
    this.setData({ show: false });
  },

  onConfirm(event){
    console.log(event)
    this.setData({ show: false });
    var that = this;
    if (event.detail.values.length > 0) {
      for (let i = 0; i < event.detail.values.length; i++) {
        if(i == 0 ){
          var province = event.detail.values[0].name;
          that.setData({
            province:province
          })
        }else if (i == 1) {
          var city = event.detail.values[1].name;
          that.setData({
            city:city
          })
        }else {
          var county = event.detail.values[2].name;
          that.setData({
            county:county
          })
        }
      }
      console.log(that.data.userInfo)
    }
  },

  onCancel() {
    this.setData({ show: false });
  },

  // 设置默认地址
  onChange({ detail }) {
    console.log(detail)
    this.setData({ checked: detail });
  },


  // 保存地址
  clickSaveAddress() {
    var that = this;
    if(that.data.userInfo.addressee.length == 0) {
      return wx.showToast({
        title: `请输入收件人姓名`,
        icon: 'none'
      });
    }

    if(that.data.userInfo.phone.length == 0) {
      return wx.showToast({
        title: `请输入电话号码`,
        icon: 'none'
      });
    }

    if(that.data.province.length == 0) {
      return wx.showToast({
        title: `请选择地区`,
        icon: 'none'
      });
    }

    if(that.data.userInfo.info.length == 0) {
      return wx.showToast({
        title: `请输入详细地址`,
        icon: 'none'
      });
    }

    if(that.data.type == 0) {
       that.addressAdd()
    }else {
      that.addressEdit()
    }
  },

  // 新增地址
  addressAdd() {
    var that = this;
    console.log(that.data.userInfo)
     request({ url: "/api/addressAdd",data:
            {province:that.data.province,
              city:that.data.city,
              county:that.data.county,
              addressee:that.data.userInfo.addressee,
              info:that.data.userInfo.info,
              phone:that.data.userInfo.phone,
              lat:that.data.userInfo.lat,
              lng:that.data.userInfo.lng,
            }})
    .then(result => {
      console.log(result)
      // 返回上一页面
      wx.navigateBack({
        delta: 1,
      })

      wx.showToast({
        title: `保存成功`,
        icon: 'none',
      });
    })
  
  },

  // 修改地址
  addressEdit() {
    var that = this;
     request({ url: "/api/addressEdit",data:
            {id:that.data.userInfo.id,
              province:that.data.province,
              city:that.data.city,
              county:that.data.county,
              addressee:that.data.userInfo.addressee,
              info:that.data.userInfo.info,
              phone:that.data.userInfo.phone
            }})
    .then(result => {
      console.log(result)
      wx.showToast({
        title: `修改成功`,
        icon: 'none',
        duration: 2000
      });
    
      setTimeout(function () {
        // 返回上一页面
       wx.navigateBack({
        delta: 1,
        })
      }, 2000)
     
    })
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    if (options.title == "添加地址") {
      wx.setNavigationBarTitle({
        title: '添加地址',
      })
      this.setData({type:0})
    }else {
      wx.setNavigationBarTitle({
        title: '修改地址',
      })
      if (options.jsonStr.length > 0) {
        let item=JSON.parse(options.jsonStr);
        this.setData({userInfo:item});
        this.setData({province:item.province});
        this.setData({city:item.city});
        this.setData({county:item.county});
        this.setData({ checked: item.is_check });
      }
      this.setData({type:1})
    }

    this.setData({areaList:areaList})
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})