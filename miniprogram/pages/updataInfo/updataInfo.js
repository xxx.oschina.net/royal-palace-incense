// pages/updataInfo/updataInfo.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    baseUrl:getApp().globalData.baseUrl,
    userUrl: getApp().globalData.baseUrl + "/images/shoukuan/",
    userInfo:{},
    fileList: [],

  },

 // 查询用户信息
 getUserInfo () {
   var that = this;
  request({ url: "/api/myInfo" })
  .then(result => {
    console.log(result)
    that.setData({
        userInfo: result
    })
  })
},  

  // 
  // addImage() {
  //   var that = this;
  //   if (that.data.hasUserInfo == false) {
  //     wx.showLoading({
  //       title: "加载中",
  //       mask: true
  //     });
  //     wx.getUserProfile({
  //       desc: '获取用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
  //       success: (res) => {
  //         wx.hideLoading();
  //         that.setData({
  //           nickName:res.userInfo.nickName,
  //           // userInfo: res.userInfo,
  //           hasUserInfo: true
  //         })
  //         that.chooseImage()
  //       },
  //       fail:(err) => {
  //         wx.hideLoading();
  //       }
        
  //     })
  //   }else {
  //     that.chooseImage()
  //   }
  
  // },

  previewImg(){

    if (this.data.userInfo.money_pic) {
      console.log(this.data.userUrl + this.data.userInfo.money_pic);
      wx.previewImage({
        current: this.data.userUrl + this.data.userInfo.money_pic,     //当前图片地址
        urls: [this.data.userUrl + this.data.userInfo.money_pic],     //所有要预览的图片的地址集合 数组形式
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {},
      })
    }
   
  },

  // 选择图片
  chooseImage() {
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: (result) => {
        console.log(result)
        this.setData({
          chooseImgs:result.tempFilePaths[0]
        })
        this.updataImg();
      }
    })
  },

  // 上传图片
  updataImg() {

    wx.showLoading({
      title: "正在上传中",
      mask: true
    });

    var that = this;
      wx.uploadFile({
      filePath: that.data.chooseImgs,
      name: 'pic',
      url: that.data.baseUrl + '/api/addMyInfo',
      header: {
        "Content-Type": "multipart/form-data",
        'Content-Type': 'application/json'    
      },
      formData: { 
        openid: wx.getStorageSync('openid')
     },
     success: (result) => {
        wx.showToast({
        title: "上传成功",
        icon: 'success',
        });
        that.getUserInfo();
      },
      fail:(err)=> {
        wx.showToast({
          title: "上传失败",
          icon: 'fail',
        });
      }
      
    })
  },




  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getUserInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})