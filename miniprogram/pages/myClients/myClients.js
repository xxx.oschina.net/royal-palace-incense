// pages/myClients/myClients.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    page:1,
    info:{}
  },

  // 查询人数计算
  getMyTeamCount() {
    var that = this;
    request({ url: "/api/myTeamCount"})
    .then(result => {
      console.log(result)
      that.setData({
        info:result
      })
    })
  },

  // 查询收益列表
  geIncomel() {
    var that = this;
    request({ url: "/api/income_log" ,data:{page:that.data.page}})
    .then(result => {
      console.log(result)
    //   for (var index in result.data) {
    //     var item = result.data[index];
    //      item.openid = item.openid.slice(-8);
    //  }
      that.setData({
        list:that.data.list.concat(result.data)
      })
    })
  },

  onTeam() {
    wx.navigateTo({
      url: '/pages/teamList/teamList',
    })

  },
  onTeamYouxiao(){
    wx.navigateTo({
      url: '/pages/teamListTwo/teamListTwo',
    })
  },
  onTeamAll() {
    wx.navigateTo({
      url: '/pages/teamAll/teamAll',
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.data.page = 1;
    this.data.list = [];
    this.getMyTeamCount()
    this.geIncomel()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.page++;
    console.log(this.page)
    this.geIncomel();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})