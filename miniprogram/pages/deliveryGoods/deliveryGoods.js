// pages/deliveryGoods/deliveryGoods.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    select: false,
    address_name: '--请选择地址--',
    addressList:[], // 地址列表
    address:{}, // 选择的地址信息
    value:"0",
    value2:"0",
  },

  bindShowMsg() {
    this.setData({
      select: !this.data.select
    })
  },
  mySelect(e) {
    console.log(e)
    var name = e.currentTarget.dataset.name
    this.setData({
      // address_name: name,
      address:name,
      select: false
    })
  },

  onChange(event){
    console.log(event)
    this.setData({
      value:event.detail
    })
  },

  onChange2(event){
    console.log(event)
    this.setData({
      value2:event.detail
    })
  },

  // 查询地址列表
  getAddressList () {
    var that = this;
    request({ url: "/api/addressList", })
    .then(result => {
      console.log(result)
      var addressList = new Array();
      addressList = result.data
      that.setData({addressList:result.data})
      if (addressList.length > 0) {
        for (var index in addressList){
          if(addressList[index].is_check == 1) {
              that.setData({
                address:addressList[index]
             })
            
          }
        }
      } else {
        console.log("未查询到地址信息")
        
      }
    })
  },

   // 发货
   onSubmit() {
    var that = this;
    request({ url: "/api/faHuo",data:{
      number1:that.data.value,
      number2:that.data.value2,
      addressId:that.data.address.id
    }})
    .then(result => {
      console.log(result)
      wx.showToast({
        title: result,
        icon: 'none',
        duration:2000
      })
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getAddressList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})