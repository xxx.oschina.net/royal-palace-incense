// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName'), // 如需尝试获取用户信息可改为false
    baseUrl:getApp().globalData.baseUrl,
    uni_code:"",
    checked:false,
    phone:"",
  },
  // 事件处理函数
  bindViewTap() {
    // console.log(111);
  
  },

  handleInput(event){
    let value = event.detail.value;
    console.log(event);
    this.setData({
      phone:value
    })
  },

  onChange(event) {
    this.setData({
      checked: event.detail,
    });
  },

  onXiyi() {
    wx.navigateTo({
      url: '/pages/xieyi/xieyi?index=0',
    })
  },

  onLogin() {


    if (this.data.phone.length != 11) {
      return wx.showToast({
        title: '请输入正确手机号',
        icon: 'none',
        duration:2000
       });
    }

    if (this.data.checked == false) {
      return wx.showToast({
        title: '请勾选用户协议',
        icon: 'none',
        duration:2000
       });
    }
   
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
        this.getLogin(res.userInfo)
      }
    })

  },

  onLoad(obj) {

    // if (wx.getUserProfile) {
    //   this.setData({
    //     canIUseGetUserProfile: true
    //   })
    // }
    // 保存uni_code
    console.log(obj.uni_code)
    wx.setStorageSync('uni_code', obj.uni_code)
    this.setData({
      uni_code:obj.uni_code
    })
     
  },

  // 登录
  getLogin(userInfo) {

    var that = this;
    wx.login({
      success: res => {
      // 发送 res.code 到后台换取 openId, sessionKey, unionId
      console.log(res)
      var uni_code = wx.getStorageSync('uni_code')
      if(res){
        wx.request({
          url: that.data.baseUrl + '/api/getOpenId',
          data:{
            code:res.code,
            uni_code:uni_code?uni_code:"",
            nickName:userInfo.nickName?userInfo.nickName:"",
            avatarUrl:userInfo.avatarUrl?userInfo.avatarUrl:"",
            phone:that.data.phone?that.data.phone:"",
          },
          success:(result)=>{
            console.log(result)
            wx.setStorageSync('openid', result.data.data)
            wx.switchTab({
              url: '/pages/home/home',
            })
            
          },
          fail:(error)=> {

          }
        })
      }
    }
    })

  },

  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
