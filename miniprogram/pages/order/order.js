// pages/order/order.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    status:0, // 当前订单状态
    orderType:[
      {
        title:"全部",
        status:"-1"
      },
      {
        title:"待付款",
        status:"0"
      },
      {
        title:"待发货",
        status:"1"
      },
      {
        title:"待收货",
        status:"2"
      },
       {
        title:"售后",
        status:"3"
      }
    ],
    orderList:[],
    goodsUrl: getApp().globalData.baseUrl + "/images/goods/",
    page:1,
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      let status = options.status;
      console.log(status)
      this.setData({
        status:parseInt(status)
      })
      // 当前订单为全部时请求
      // if (status == 0) {
      //   this.getOrderList("-1")
      // }
  },

  // 点击订单状态
  // onChange(event) {
  //   console.log(event);
  //   let title = event.detail.title;
  //   if (title == '待付款') {
  //     this.getOrderList("0")
  //   } else if (title == '待发货') {
  //     this.getOrderList("1")
  //   } else if (title == '待收货') {
  //     this.getOrderList("2")
  //   } else if (title == '售后') {
  //     this.getOrderList("3")
  //   }else {
  //     this.getOrderList("-1")
  //   }
  //   this.setData({status:event.detail.index})
  //   console.log(this.data.status);
  // },

  // 去付款
  onPay(event) {
    let value= event.currentTarget.dataset.value
    console.log("value："+value)
    this.getBuyNow(value)
  },

  // 点击查看物流
  onLogistics() {
    console.log(111)
    wx.navigateTo({
      url: '/pages/logistics/logistics',
    })
  },

  // 点击确认收货
  onTakeGoods (){
    console.log(111)
  },
  
  //  查询订单数据
  getOrderList (status) {
    var that = this;
    request({ url: "/api/orderList", data:{status:status,page:that.data.page}})
    .then(result => {
      console.log(result)
      that.setData({
        orderList:that.data.orderList.concat(result.data)
      })
      wx.stopPullDownRefresh();
    })
  },

   // 立即购买
   getBuyNow (order) {
    var that = this;
    request({ url: "/api/buyNow",
        data:{
          orderId:order.id,
          addressId:order.addressId
        
        } 
    })
    .then(result => {
      console.log(result)
      wx.requestPayment({
        nonceStr: result.nonceStr,
        package: result.package,
        paySign: result.paySign,
        timeStamp: result.timeStamp,
        signType: result.signType,
        success (res){
          console.log(res)
          wx.showToast({
            title: "支付成功",
            icon: 'none',
            duration:2000
          });
        },
        fail(err) {
          console.log(err)
          wx.showToast({
            title: "支付失败",
            icon: 'none',
            duration:2000
          });
        }
      })
      
    })
  },



  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.data.page = 1;
    this.data.orderList = [];
    if (this.data.status == 1) {
      this.getOrderList("0")
    } else if (this.data.status == 2) {
      this.getOrderList("1")
    } else if (this.data.status == 3) {
      this.getOrderList("2")
    } else if (this.data.status == 4) {
      this.getOrderList("3")
    }else {
      this.getOrderList("-1")
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    wx.switchTab({
      url: '/pages/mine/mine'
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.data.page = 1;
    this.setData({
      orderList:[]
    })
    if (this.data.status == 1) {
      this.getOrderList("0")
    } else if (this.data.status == 2) {
      this.getOrderList("1")
    } else if (this.data.status == 3) {
      this.getOrderList("2")
    } else if (this.data.status == 4) {
      this.getOrderList("3")
    }else {
      this.getOrderList("-1")
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.page++;
    // var type = this.data.orderType[this.data.status];
    // this.getOrderList(type.status)
    if (this.data.status == 1) {
      this.getOrderList("0")
    } else if (this.data.status == 2) {
      this.getOrderList("1")
    } else if (this.data.status == 3) {
      this.getOrderList("2")
    } else if (this.data.status == 4) {
      this.getOrderList("3")
    }else {
      this.getOrderList("-1")
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})