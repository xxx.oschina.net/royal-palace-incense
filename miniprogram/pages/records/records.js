// pages/records/records.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    page:1,
    status:0, 
    goodsUrl: getApp().globalData.baseUrl + "/images/goods/",
  },

  // 网络请求
  getRequest() {

    if (this.data.status == 0) { // 库存记录
      this.getRecordsList("/api/stockList")
    } else if (this.data.status == 1) { // 升级记录
      this.getRecordsList("/api/upgradeList")
    } else if (this.data.status == 2) { // 复购记录
      this.getRecordsList("/api/fuGouList") 
    } else if (this.data.status == 3) { // 查询发货记录
      this.getRecordsList("/api/faHuoList")
    } else if (this.data.status == 4) { // 查询转盘记录
      this.getRecordsList("/api/turnList")
    } else if (this.data.status == 5) { // 查询特惠记录
      this.getRecordsList("/api/onSaleList")
    } else if (this.data.status == 6) { // 查询收益记录
      this.getRecordsList("/api/income_log")
    }else {
      
    }
  },

  // 查询记录
  getRecordsList (url) {
    var that = this;
    request({ url: url ,data:{page:that.data.page}})
      .then(result => {
        console.log(result)
        that.setData({
          list:that.data.list.concat(result.data)
       })
    })
  },

  // 设置标题
  setNavTitle(title) {
    wx.setNavigationBarTitle({
      title: title,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let status = options.status;
    console.log(status)
    this.setData({
      status:parseInt(status)
    })
    if (this.data.status == 0) { // 库存记录
      this.setNavTitle("库存记录")
    } else if (this.data.status == 1) { // 升级记录
      this.setNavTitle("升级记录")
    } else if (this.data.status == 2) { // 复购记录
      this.setNavTitle("复购记录") 
    } else if (this.data.status == 3) { // 发货记录
      this.setNavTitle("发货记录")
    } else if (this.data.status == 4) { // 转盘记录
      this.setNavTitle("转盘记录")
    } else if (this.data.status == 5) { // 特惠记录
      this.setNavTitle("特惠记录")
    } else if (this.data.status == 6) { // 收益记录
      this.setNavTitle("收益记录")
    }

  },

  onFahuo(item){
    var orderId = item.currentTarget.dataset.value.id;
      wx.navigateTo({
        url: '/pages/logistics/logistics?orderId='+orderId+'&type=1',
      })
  },

  onFahuo2(item){
    var orderId = item.currentTarget.dataset.value.id;
      wx.navigateTo({
        url: '/pages/logistics/logistics?orderId='+orderId+'&type=2',
      })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.data.page = 1;
    this.data.list = [];
    this.getRequest()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.page++;
    this.getRequest()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})