// pages/logistics/logistics.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsUrl: getApp().globalData.baseUrl + "/images/goods/",
    active:0,
    orderId:"",
    steps: [],
    map:{},
    code:"",
    longitude: '',
    latitude:'',
    points: [],
    polyline: [],
    markers:[{
      latitude: 40.2207700,
      longitude: 116.2312800
    },
    {
      latitude: 34.7246800,
      longitude: 113.6401000
    }
    ],
    type:1,
  },

  // 查询物流信息
  queryEms(){
      request({ url: "/api/queryEms",data:{orderId:this.data.orderId}})
    .then(result => {
      console.log(result)
      var list = new Array;
      for (var index in result.map.Traces) {
        var traces =  result.map.Traces[index];
        var item = {
          text: traces.AcceptStation,
          desc: traces.AcceptTime
        }
        list.push(item);
      }
      list.sort(); 
      list.reverse();
      this.setData({
        steps:list
      })

      this.setData({
        map:result.map
      })
      this.getEmsName(this.data.map.ShipperCode);
  
    })
  },

   // 查询物流信息
   queryEms2(){
    request({ url: "/api/queryEms2",data:{orderId:this.data.orderId}})
  .then(result => {
    console.log(result)
    var list = new Array;
    for (var index in result.map.Traces) {
      var traces =  result.map.Traces[index];
      var item = {
        text: traces.AcceptStation,
        desc: traces.AcceptTime
      }
      list.push(item);
    }
    list.sort(); 
    list.reverse();
    this.setData({
      steps:list
    })

    this.setData({
      map:result.map
    })
    this.getEmsName(this.data.map.ShipperCode);

  })
},

   // 查询物流公司
   getEmsName (code) {
    request({ url: "/api/getEmsName",data:{code:code} })
    .then(result => {
      this.setData({
        code: result
      })
    })
  },


  click: function (e) {
    
  },

  onMap(){
    var code = this.data.map.RouteMapUrlCode;
    wx.navigateTo({
      url: '/pages/webView/webView?code='+code, 
    })
  },

  // 复制运单编码
  onCopy(){
    wx.setClipboardData({
      data:this.data.map.OrderCode,
      success:function (res) {
        wx.getClipboardData({
          success:function (res) {
            // console.log(res.data)// data
          }
        })
      }
    })
  },

  // 拨打电话
  onCallPhone(){
    if (this.data.map.DeliveryManTel) {
      wx.makePhoneCall({
        phoneNumber: this.data.map.DeliveryManTel,
      })
    }else {
      wx.showToast({
        title: "暂无信息",
        icon: 'none',
        duration:2000
        });
    }
   
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var orderId = options.orderId;
    var type = options.type;
    this.setData({
      orderId:orderId,
      type:type
    })
    if (this.data.type == 1) {
      this.queryEms();
    }else {
      this.queryEms2();
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})