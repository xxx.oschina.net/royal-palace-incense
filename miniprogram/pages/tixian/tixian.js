// pages/tixian/tixian.js
import { request } from "../../utils/index.js";
import { request1 } from "../../utils/request.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    price:"",
    userInfo:{},
  },

  // 查询用户信息
  getUserInfo () {
   var that = this;
   request1({ url: "/api/myInfo" })
    .then(result => {
    console.log(result)
    that.setData({
        userInfo: result
    })
    })
  }, 


   // 提现操作
   getWithdraw () {
    var that = this;

    if(!that.data.price) {
      return wx.showToast({
        title: '请输入提现金额',
        icon: 'none',
        duration:2000
        });
    }

    request({ url: "/api/withdraw" ,data:{price:that.data.price}})
    .then(result => {
      console.log(result)
      wx.navigateTo({
        url: '/pages/txRecords/txRecords',
      })
      that.getUserInfo();
      
    })
  },

  // 输入提现金额
  onChange(event) {
    console.log(event)
    var price = event.detail;
    this.setData({
      price:price
    })
  },

  // 跳转提现记录页面
  toRecords() {
    wx.navigateTo({
      url: '/pages/txRecords/txRecords',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getUserInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})