// pages/mine/mine.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    baseUrl:getApp().globalData.baseUrl,
    userInfo:{},
    chooseImgs:"",
    hasUserInfo:false,
    nickName:""
  },

  // 点击全部订单
  onAllOrder(){
    wx.navigateTo({
      url: '/pages/order/order?status=0',
    })
  },

  // 查询用户信息
  getUserInfo () {
    var that = this;
    request({ url: "/api/myInfo" })
    .then(result => {
      console.log(result)
      that.setData({
          userInfo: result
      })
      let uni_code = that.data.userInfo.uni_code;
      wx.setStorageSync('uni_code', uni_code)
    })
  },

  updataInfo(){
    wx.getUserProfile({
     desc: '获取用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        request({url: "/api/getNickName",data:
          {
            nickName:res.userInfo.nickName,
            avatarUrl:res.userInfo.avatarUrl
          }
        })
        .then(result => {
          wx.showToast({
            title: result,
            icon: 'none',
            duration:2000
          });
        })
      },
      fail:(err) => {

      }  
    })   
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getUserInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

 

 
})