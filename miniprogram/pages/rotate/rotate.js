// pages/rotate/rotate.js
import { request } from "../../utils/index.js";
const app = getApp()
var animation;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rotateSwitch:true,
    // prizeList: ['5元', '10元', '15元', '20元', '5元', '10元'],
    // color: ["#F52B54", "#FFF8D7","#F52B54", "#FFF8D7","#F52B54", "#FFF8D7"],//扇形的背景颜色交替；
    prizeList: ['../../images/turntable/5.png', '../../images/turntable/10.png', '../../images/turntable/15.png', '../../images/turntable/20.png', '../../images/turntable/52.png', '../../images/turntable/10_2.png'],
    color: ["-webkit-linear-gradient(top,#ffde5a,#FFFFFF)", "#FFFFFF","-webkit-linear-gradient(top,#ffde5a,#FFFFFF)", "#FFFFFF","-webkit-linear-gradient(top,#ffde5a,#FFFFFF)", "#FFFFFF"],//扇形的背景颜色交替；
    rotateInfo:{},
    show: false,
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.initAwardList(this.data.prizeList);
  },

  /**
   * 初始化抽奖
   */
  initAwardList: function(list) {
    // 绘制转盘
    var awardsList = [];
    var angle = 360 / list.length;
    for (var i = 0; i < list.length; i++) {
      awardsList.push({
        degree: {
          item: `${i * angle}deg`,
          line: `${(i + 0.5) * angle}deg`,
          bg_1: `${(i - 0.5) * angle + 90}deg`,
          bg_2: `${angle - 90}deg`
        },
        award: list[i],
        color: this.data.color[i]
      })
    }
    this.setData({
      awardsList: awardsList
    });
  },

  /**
   * 开始抽奖
   */
  getLottery: function() {
    let that=this

    request({ url: "/api/turntable" })
    .then(result => {
      console.log(result)
      that.setData({
        rotateInfo:result
      })
      if(result.id == "1") { //5元
           /// 获奖序号
        var awardIndex = 0;
      }else if (result.id == "2") { //10元
          /// 获奖序号
       var awardIndex = 1;
      }else if (result.id == "3") { //15元
          /// 获奖序号
        var awardIndex = 2;
      }else if (result.id == "4") { //20元
          /// 获奖序号
        var awardIndex = 3;
      }else if (result.id == "5") { //5元
          /// 获奖序号
        var awardIndex = 4;
      }else if (result.id == "6"){ //10元
          /// 获奖序号
        var awardIndex = 5;
      }

    
    /// 转动时间
    let duration = 4000;
    /// 转动圈数
    let runNum = 6;

    // 旋转抽奖
    this.runDegs = this.runDegs || 0;
    this.runDegs = this.runDegs + (360 - this.runDegs % 360) + (360 * runNum - awardIndex * (360 / this.data.awardsList.length));

    /// 开始转动
    this.startRun(awardIndex, duration, this.runDegs);
    })

   
  },

  /**
   * 开始转动
   */
  startRun: function(awardIndex, duration, runDegs) {
    /// 动画
    var animation = wx.createAnimation({
      duration: duration,
      timingFunction: 'ease'
    })
    animation.rotate(runDegs).step();
    this.setData({
      animationData: animation.export()
    })

    /// 中奖提示
    setTimeout(() => {
      this.stopRun(awardIndex);
    }, duration + 300)
  },

  /**
   * 结束转动
   */
  stopRun: function(awardIndex) {
    console.log('抽中了奖品: ' + awardIndex)
    let that=this
    that.setData({ show: true });

  },

    // 加入拼图
    onJoin() {
      this.getAddTuan()
    },
  
     // 查询加入拼团
     getAddTuan () {
      var that = this;
      request({ url: "/api/addTuan"})
      .then(result => {
        console.log(result)
        wx.showToast({
          title: result,
          icon: 'none',
          duration:3000
         });
        wx.navigateTo({
          url: '/pages/groupBuy/groupBuy',
        })

      })
    },
  
    confirm(event) {
      console.log(event.detail);
      this.getOrderNow();
    },
  
    onClose() {
      this.setData({ show: false });
    },
  
      // 下单接口
      getOrderNow () {
        var that = this;
        request({ url: "/api/orderNow",
            data:{
                  type:"1",
                  goodsId:"22",
                  price:that.data.rotateInfo.price,
                  number:"1"
                 } 
        })
        .then(result => {
          console.log("订单id +++ ", result)
           that.getBuyNow(result)
        })
      },
  
       // 立即购买
    getBuyNow (orderId) {
      var that = this;
      request({ url: "/api/buyNow",
          data:{
            orderId:orderId,
            addressId:"-1",
            } 
      })
      .then(result => {
        console.log(result)
        wx.requestPayment({
          nonceStr: result.nonceStr,
          package: result.package,
          paySign: result.paySign,
          timeStamp: result.timeStamp,
          signType: result.signType,
          success (res){
            console.log(res)
            wx.showToast({
              title: "支付成功",
              icon: 'none',
              duration:2000
            });
          },
          fail(err) {
            console.log(err)
            wx.showToast({
              title: "支付失败",
              icon: 'none',
              duration:2000
            });
          }
        })
        
      })
    },
      
    toRecords(){
      wx.navigateTo({
        url: '/pages/xieyi/xieyi?index=1',
      })
    },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})