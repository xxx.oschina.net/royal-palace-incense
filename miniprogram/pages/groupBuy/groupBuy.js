// pages/groupBuy/groupBuy.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    page:1,
  },

  // 查询加入拼团
  getTuanInfo () {
    var that = this;
    request({ url: "/api/tuan_info" ,data:{page:that.data.page}})
    .then(result => {
      console.log(result)
      for (var index in result.data) {
         var item = result.data[index];
          item.openid = item.openid.slice(-8);
      }
      that.setData({
        list:that.data.list.concat(result.data)
      })
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.data.page = 1;
    this.data.list = [];
    this.getTuanInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.page++;
    this.getTuanInfo();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})