// pages/bindPhone/bindPhone.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    value:'',
    userInfo:{},
  },

   // 查询用户信息
   getUserInfo () {
    var that = this;
    request({ url: "/api/myInfo" })
    .then(result => {
      console.log(result)
      that.setData({
          userInfo: result
      })
    
    })
  },

  handleInput(event) {
    let value = event.detail.value;
    console.log(event);
    this.setData({
        value:value
    })

  },

    // 绑定手机
    clickBind() {

    var that = this
    if (that.data.value.length == 0) {
      return  wx.showToast({
        title: '请输入手机号',
        icon: 'none',
        duration:2000
      });
    }
   console.log(that.data.value)
    request({ url: "/api/binding_phone",data:{phone:that.data.value} })
    .then(result => {
      wx.showToast({
        title: result,
        icon: 'none',
        duration:2000
      });
      this.getUserInfo()
    })


    },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getUserInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})