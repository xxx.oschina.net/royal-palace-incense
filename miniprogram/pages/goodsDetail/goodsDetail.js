// pages/goodsDetail/goodsDetail.js
import { request } from "../../utils/index.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    value: 1,
    goods:{},
    goodsUrl: getApp().globalData.baseUrl + "/images/goods/",
    number:1,
    goodsId:"",
  },

  // 点击购买
  clickBuyBtn() {
    this.setData({ show: true });
  },

  // 关闭
  onClose() {
    this.setData({ show: false });
  },

  // 编辑商品数量
  onChange(event) {
    console.log(event.detail);
    this.setData({number:event.detail});
  },

  // 点击完成
  onSubmit() {
    var that = this;
    let str=JSON.stringify(that.data.goods);
    wx.navigateTo({
      url: '/pages/submitOrder/submitOrder?number='+that.data.number+'&jsonStr='+encodeURIComponent(str)
    })
  },

  // 点击分享
  onShare() {
    console.log("点击分享")
  },

  // 查询商品详情
  getGoodsInfo (goodsId) {
    request({ url: "/api/goodsInfo",data:{
      goodsId:goodsId} 
    })
    .then(result => {
      console.log(result)
      this.setData({
        // goods: result
        goods:{
          id:result.id,
          title:result.title,
          pic:result.pic,
          price:result.price,
          content:result.content,
          sort:result.sort,
          stock:result.stock,
          info:result.info.replace(/\<img/gi,'<img style="max-width:100%;height:auto"')
        }
      })
      console.log(this.data.goods)
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    let goodsId = options.goodsId;
    this.getGoodsInfo(goodsId);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})