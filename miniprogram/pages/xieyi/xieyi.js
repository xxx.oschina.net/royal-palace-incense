// pages/xieyi/xieyi.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:{},
    index:0,
  },

  getXieyi () {
    var that = this;
    request({ url: "/api/xieyiList" })
    .then(result => {
      console.log(result)
      if (that.data.index == 0){
        wx.setNavigationBarTitle({
          title: result[1].title,
        })
  
        that.setData({
          info: {
            title:result[1].title,
            content:result[1].content.replace(/\<img/gi,'<img style="max-width:100%;height:auto"')
          }
        })
      }else {
        wx.setNavigationBarTitle({
          title: result[0].title,
        })
  
        that.setData({
          info: {
            title:result[0].title,
            content:result[0].content.replace(/\<img/gi,'<img style="max-width:100%;height:auto"')
          }
        })
      }
     
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
      this.setData({
        index:options.index
      });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getXieyi()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})