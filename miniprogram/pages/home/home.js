// pages/home.js
import { request } from "../../utils/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
      imgUrl: getApp().globalData.baseUrl + "/images/banner/",
      categoryUrl: getApp().globalData.baseUrl + "/images/category/",
      goodsUrl: getApp().globalData.baseUrl + "/images/goods/",
      bannerList:[], 
      noticeList:[],
      categoryList:[],
      goodsList : []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {    
     //轮播图数据
    this.getSwiperList()
    // 公告通知
    this.getNoticeList()
    // 分类数据
    this.getCategoryList()
    // 商品列表数据
    this.getGoodsList()
  },

  getSwiperList () {
    request({ url: "/api/banner" })
    .then(result => {
      this.setData({
        bannerList: result
      })
    })
  },

  getNoticeList () {
    request({ url: "/api/noticeList" })
    .then(result => {
      this.setData({
        noticeList: result
      })
    })
  },

  getCategoryList () {
    request({ url: "/api/categoryList" })
    .then(result => {
      this.setData({
        categoryList: result
      })
    })
  },

  getGoodsList () {
    request({ url: "/api/goodsList" })
    .then(result => {
      this.setData({
        goodsList: result.data
      })
    })
  },

  // 点击产品推荐
  onItmeClick (item){
    console.log(item)
    // let str=JSON.stringify(item.currentTarget.dataset.value);
    let goodsId = item.currentTarget.dataset.value.id;
    wx.navigateTo({
      // url: '/pages/goodsDetail/goodsDetail?jsonStr='+encodeURIComponent(str)
      url: '/pages/goodsDetail/goodsDetail?goodsId='+goodsId
    })
  },

  // 公告通知
  onNotice(event) {
    console.log(event)
    let str=JSON.stringify(event.currentTarget.dataset.value);
    wx.navigateTo({
      url: '/pages/notice/notice?jsonStr='+encodeURIComponent(str), 
    })
  },

  
  // 点击分类
  onCategory(item){
    console.log(item);
    let str=JSON.stringify(item.currentTarget.dataset.value.id);
    wx.navigateTo({
      url: '/pages/category/category?id='+str, 
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})