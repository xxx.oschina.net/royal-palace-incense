// pages/submitOrder/submitOrder.js
import { request } from "../../utils/index.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    value:"",// 备注信息
    number:1,// 商品数量
    goods:{},// 商品信息
    goodsUrl: getApp().globalData.baseUrl + "/images/goods/",
    addressList:[], // 地址列表
    address:{} // 选择的地址信息
  },

  // 输入备注
  onChange(event){
    console.log(event)
    this.setData({
      value:event.detail
    })
  },

  // 点击提交订单
  onSubmit() {

    if(!this.data.address.id) {
     return wx.showToast({
      title: `请添加地址`,
      icon: 'none',
      });
    }

    this.getOrderNow()
  },

    // 下单接口
  getOrderNow () {
    var that = this;
    request({ url: "/api/orderNow",
        data:{
              type:"0",
              goodsId:that.data.goods.id,
              price:that.data.goods.price,
              number:that.data.number
             } 
    })
    .then(result => {
      console.log("订单id +++ ", result)
       that.getBuyNow(result)
    })
  },


  // 立即购买
  getBuyNow (orderId) {
    var that = this;
    request({ url: "/api/buyNow",
        data:{
          orderId:orderId,
          addressId:that.data.address.id,
             } 
    })
    .then(result => {
      console.log(result)
      wx.requestPayment({
        nonceStr: result.nonceStr,
        package: result.package,
        paySign: result.paySign,
        timeStamp: result.timeStamp,
        signType: result.signType,
        success (res){
          console.log(res)
          wx.showToast({
            title: "支付成功",
            icon: 'none',
            duration:2000
          });
          wx.navigateTo({
            url: '/pages/order/order?status=2'
          })
   
        },
        fail(err) {
          console.log(err)
          wx.showToast({
            title: "支付失败",
            icon: 'none',
            duration:2000
          });
        }
      })
      
    })
  },

  // 查询地址列表
  getAddressList () {
    var that = this;
    request({ url: "/api/addressList", })
    .then(result => {
      console.log(result)
      var addressList = new Array();
      addressList = result.data
      that.setData({addressList:result.data})
      if (addressList.length > 0) {
        for (var index in addressList){
          if(addressList[index].is_check == 1) {
              that.setData({
                address:addressList[index]
             })
          }
        }
      } else {
        console.log("未查询到地址信息")
        
      }
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    let goods=JSON.parse(decodeURIComponent(options.jsonStr));
    let number = options.number;
    this.setData({goods:goods,number:number});

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 查询地址列表
    this.getAddressList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})