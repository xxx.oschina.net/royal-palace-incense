// components/GoodsItem/GoodsItem.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title:{
      type:String,
      value:'title'
    },
    image:{
      type:String,
      value:'image'
    },
    price:{
      type:String,
      value:'price'
    },
    number:{
      type:String,
      value:'number'
    },
   

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
